<?php

namespace Drupal\tikitoki\FieldProcessor;

/**
 * Class TextFieldProcessor.
 *
 * @package Drupal\tikitoki\FieldProcessor
 */
class TextFieldProcessor extends BaseFieldProcessor {
  /**
   * {@inheritdoc}
   */
  protected static $destinationId = 'text';

}
