<?php

namespace Drupal\tikitoki\FieldProcessor;

/**
 * Class TitleFieldProcessor.
 *
 * @package Drupal\tikitoki\FieldProcessor
 */
class TitleFieldProcessor extends BaseFieldProcessor {
  /**
   * {@inheritdoc}
   */
  protected static $destinationId = 'title';

}
